var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var materialsRouter = require('./routes/materials');
var borrowsRouter = require('./routes/borrows');
var reservationsRouter = require('./routes/reservations');

var db = require("./handler/db");
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: 'secret', saveUninitialized: true, resave: true }));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/materials', materialsRouter);
app.use('/borrows', borrowsRouter);
app.use('/reservations', reservationsRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');

  //boostrap setup
  app.use('/css', express.static(path.join(__dirname + 'node_modules/bootstrap/dist/css')));
  app.use('/js', express.static(path.join(__dirname + 'node_modules/bootstrap/dist/js')));
});


module.exports = app;
