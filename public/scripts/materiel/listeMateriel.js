/*
*	Ce script permet d'afficher tous les appareils dans la page listeMateriel
*/

$(document).ready(function() {
	refresh();
});

function refresh() {
	sideMenuLogic();
	pageInit();
	populateTable();
}

function pageInit() {
	//Si l'utilisateur n'est un administrateur on cache le bouton nouveau matériel
	$("#creerMaterielLien").hide();
	if(sessionStorage.getItem("role") === "administrator") {
		$("#creerMaterielLien").show();
	}
}

//remplit la table avec les bonnes informations
function populateTable() {

	let tableContent = '';

	$.getJSON('/materials/readMaterials', function(data) {
		$.each(data, function() {
			//Définit le lien cliquable pour rediriger vers les détails d'un appareil
			tableContent+='<tr class=\'clickable-row\' data-href=\'/materiel/'+ this._id +'\'>';
			tableContent+='<td>'+ this.name +'</td>';
			tableContent+='<td>'+ this.version +'</td>';
			tableContent+='<td>'+ this.reference +'</td>';
			if (this.borrowed) {
				tableContent+='<td> OUI </td>';
			} else {
				tableContent+='<td> NON </td>';
			}
			tableContent += '</tr>';
		});
		$('#tbodyMaterials').html(tableContent);
		//Fait de chaque ligne un lien cliquable
		$(".clickable-row").click(function() {
	        window.location = $(this).data("href");
	    });
	});
}
