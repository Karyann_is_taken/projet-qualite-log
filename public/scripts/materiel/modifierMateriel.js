/*
 *	Ce script gère la page qui permet à un administrateur de modifier et supprimer un matériel
 */

$(document).ready(function() {
  refresh();
});

function refresh() {
  sideMenuLogic();
  getMateriel();
}

let currentMaterial = '';

//Récupère les informations liées à un appareil
function getMateriel() {
	let url = '/materials/readMaterial/' + ($('#pGetID').text());
	$.getJSON(url, function (data) {
		currentMaterial = data;
		$("#inputNom").val(data.name);
		$("#inputVersion").val(data.version);
		$("#inputReference").val(data.reference);

		if (data.tel_number != null) {
			$("#inputNumTel").val(data.tel_number);
		} else {
			$("#inputNumTel").val('');
		}
	});
}

$("#validerBoutton").on("click", modifierMateriel);

//Modifie un appareil
function modifierMateriel() {
	event.preventDefault();
	$(".error").remove();

	let nbEmptyFields = 0;

	//Vérification des champss
	$('input,textarea,select').filter('[required]:visible').each(function (index, val) {
		if($(this).val() == '') {
			nbEmptyFields++;
		}
	});

	//Vérification des regex numéro de téléphone
	var regExTelNum = /^([0-9]){10}$/;
	var validTelNum = regExTelNum.test($('#inputNumTel').val());
	if (!validTelNum && $('#inputNumTel').val() != '') {
		$('#inputNumTel').after('<span class="error">Entrez un numéro de téléphone valide (10 chiffres)</span>');
	}

	//Vérification des regex version
	var regExVersion = /^([a-zA-Z0-9_-]){3,15}$/;
	var validVersion = regExVersion.test($('#inputVersion').val());
	if (!validVersion) {
		$('#inputVersion').after('<span class="error">Entrez un numéro de version valide (entre 3 et 15 caractères alpha-numériques)</span>');
	}

	//Vérification des regex référence
	var regExRef = /^(AN|AP|XX)\d\d\d$/;
	var validRef = regExRef.test($('#inputReference').val());
	if (!validRef) {
		$('#inputReference').after('<span class="error">Entrez un AN,AP ou XX puis 3 chiffres (AN485 par exemple)</span>');
	}


	//Si les champs sont valides et les champs requis sont remplis crée la requête pour créer un nouveau appareil
	if (nbEmptyFields === 0 && (validTelNum || $('#inputNumTel').val() === '') && validVersion && validRef) {
		var newMateriel = {
			'name': $("#inputNom").val(),
			'version': $("#inputVersion").val(),
			'reference': $("#inputReference").val(),
			'photo': currentMaterial.photo,
			'tel_number': $("#inputNumTel").val(),
			'borrowed': currentMaterial.borrowed
		};

		let url = '/materials/updateMaterial/'+$('#pGetID').text();

		$.post(url, newMateriel, function(data) {
			alert("Matériel modifié");
			getMateriel();
		});

	} else {
		if (nbEmptyFields != 0) {
			alert('Remplissez les champs Nom, Version, Reference au minimum');
		}
		return false;
	}
}

$("#delBouton").on("click", supprimerMateriel);
//Supprime un appareil
function supprimerMateriel() {
	let confirmation = confirm("Voulez-vous vraiment supprimer ce matériel ?");
	if (confirmation) {
		$.ajax({
			type: 'DELETE',
			url: '/materials/deleteMaterial/'+currentMaterial._id
		}).done(function( response ) {
			window.location.replace("/listeMateriel");
		});
	} else {
		return false;
	}
}
