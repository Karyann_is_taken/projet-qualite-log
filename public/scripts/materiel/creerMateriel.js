/*
* Ce script gère la page qui permet a un administrateur d'enregistrer un nouveau matériel
*/

$(document).ready(function() {
  refresh();
});

function refresh() {
  sideMenuLogic();
}

$("#validerBoutton").on("click", ajouterMateriel);

//Créer un matériel
function ajouterMateriel() {
	event.preventDefault();
	$(".error").remove();

	let nbEmptyFields = 0;

	//Compte le nombre de champs "required" vides dans le formulaires
	$('input,textarea,select').filter('[required]:visible').each(function (index, val) {
		if($(this).val() == '') {
			nbEmptyFields++;
		}
	});

	//Validation regex numéro de téléphone
	var regExTelNum = /^([0-9]){10}$/;
	var validTelNum = regExTelNum.test($('#inputNumTel').val());
	if (!validTelNum && $('#inputNumTel').val() != '') {
		$('#inputNumTel').after('<span class="error">Entrez un numéro de téléphone valide (10 chiffres)</span>');
	}

	//Validation regex version
	var regExVersion = /^([a-zA-Z0-9_-]){3,15}$/;
	var validVersion = regExVersion.test($('#inputVersion').val());
	if (!validVersion) {
		$('#inputVersion').after('<span class="error">Entrez un numéro de version valide (entre 3 et 15 caractères alpha-numériques)</span>');
	}

	//Validation regex référence
	var regExRef = /^(AN|AP|XX)\d\d\d$/;
	var validRef = regExRef.test($('#inputReference').val());
	if (!validRef) {
		$('#inputReference').after('<span class="error">Entrez un AN,AP ou XX puis 3 chiffres (AN485 par exemple)</span>');
	}

	//Si tous les champs requis sont remplis et que les champs sont valides, crée la requête POST
	if (nbEmptyFields === 0 && (validTelNum || $('#inputNumTel').val() === '') && validVersion && validRef) {
		var newMateriel = {
			'name': $("#inputNom").val(),
			'version': $("#inputVersion").val(),
			'reference': $("#inputReference").val(),
			'borrowed': false
		};

		if ($("#inputPhoto").val() !== '') {
			newMateriel.photo =  $("#inputPhoto").val();
		}

		if ($("#inputNumTel").val() !== '') {
			newMateriel.tel_number =  $("#inputNumTel").val();
		}

		console.log(newMateriel);

		$.post('/materials/addMaterial', newMateriel, function(data) {
			alert("Matériel crée");
			$('#formMateriel input').val('');
		});

	} else {
		if (nbEmptyFields != 0) {
			alert('Remplissez les champs Nom, Version, Reference au minimum');
		}
		return false;
	}
}
