/*
 *	Ce script permet de visualiser les informations d'un appareil
 *  Un utilisateur connecté peut accèder à la page pour voir les réservations associées à l'appareil
 *  Un administrateur peut voir les boutons pour valider l'emprunt du matériel par un utilisateur qu'il choisira ou confirmer qu'un matériel à été rendu
 */

let materielEstEmprunte = false;

function materielInit() {
	//On cache les boutons pour modifier et gérer l'emprunt d'un appareil
	$("#lienModifier").hide();
	$("#lienEmprunterMateriel").hide();
	$("#lienFinEmprunt").hide();

	//Si l'utilisateur est un administrateur
	if(sessionStorage.getItem("role") === "administrator") {
		//On affiche le bouton pour la page pour modifier le matériel
		$("#lienModifier").show();
		//Si le matériel est emprunté on affiche le bouton pour confirmer la fin d'un emprunt, sinon on affiche le bouton pour confirmer le rendu d'un appareil
		if(materielEstEmprunte) {
			$("#lienFinEmprunt").show();
		} else {
			$("#lienEmprunterMateriel").show();
		}
	}
	//Si l'utilisateur n'est pas connecté on cache le bouton pour afficher les réservations associéesà un appareil
	if(sessionStorage.getItem("idUser") === "undefined" || sessionStorage.getItem("idUser") === null) {
		$("#lienListeReservation").hide();
	}
}

$(document).ready(function() {
	refresh();
});

function refresh() {
	sideMenuLogic();
	getMateriel();

}

//Récupère les informations liés au matériel
function getMateriel() {
	let url = '/materials/readMaterial/' + ($('#pGetID').text());
	$.getJSON(url).done(function (data) {
		$("#nomMateriel").text("Nom : " + data.name);
		$("#versionMateriel").text("Version : " + data.version);
		$("#refMateriel").text("Référence : " + data.reference);

		if (data.tel_number != null) {
			$("#numtelMateriel").text("Numéro de téléphone :" + data.tel_number);
		} else {
			$("#numtelMateriel").hide();
		}
		//Modifie la checkboxEmprunte en fonction de si l'appareil est emprunté
		if(data.borrowed) {
			materielEstEmprunte = true;
			$("#checkboxEmprunte").prop('checked', true);
		} else {
			materielEstEmprunte = false;
			$("#checkboxEmprunte").prop('checked', false);
		}

		//Si le matériel n'a pas de photo on cache
		if(data.photo != null) {
			$("#imgMateriel").text("Image : "+ data.photo);
		} else {
			$("#imgMateriel").hide();
		}
		//Set le lien pour les boutons modifier, réservation et emprunt du matériel
		let urlMod = "/materiel/"+ ($('#pGetID').text()) +"/modifierMateriel";
		$("#lienModifier").attr("href", urlMod);

		let urlRes = "/materiel/"+ ($('#pGetID').text()) +"/reservation";
		$("#lienListeReservation").attr("href", urlRes);

		let urlValEmprunt = "/materiel/"+ ($('#pGetID').text()) +"/emprunt";
		$("#lienEmprunterMateriel").attr("href", urlValEmprunt);

		materielInit();

	});
}

$("#buttonFinEmprunt").on("click", finEmprunt);
//Fin d'un emprunt
function finEmprunt() {
	let confirmation = confirm("Valider la fin de l'emprunt du matériel ?");
	if (confirmation) {
		var mEmprunt = {
			'material_id': $("#pGetID").text(),
		};

		$.post('/borrows/returnMaterial', mEmprunt, function(data) {
			alert("Rendu !");
			window.location.replace("/materiel/"+$("#pGetID").text());
		});
	} else {
		return false;
	}
}
