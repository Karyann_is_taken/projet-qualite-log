/*
 * Ce script gère l'authentification
 */

$(document).ready(function() {
  refresh();
});

function refresh() {
  sideMenuLogic();
}

$("#connecterBoutton").on("click", connect);

function connect(event) {
	event.preventDefault();
	let nbEmptyFields = 0;

	//Vérifie si des champs sont vides
	$("#formUserConnexion input").each(function (index, val) {
		if($(this).val() == '') {
			nbEmptyFields++;
		}
	});

	//Si tous les champs sont remplis
	if (nbEmptyFields === 0) {
		var logins = {
			'mail_address': $("#inputMail").val(),
			'password': $("#motdepasse").val()
		};
		//Reqûete pour l'authentification
		$.ajax({
			type: "POST",
			data: logins,
			url: '/identification',
			dataType: 'JSON',
			success: function(data) {
				if(data.responseText === 'succes') {
					alert("Vous êtes connecté");
				} else {
					console.log("Identifiants invalides");
				}
			},
			error: function (dataError) {
				if(dataError.responseText === 'succes') {
					alert("Vous êtes connecté");
					document.location.href = '/';
				} else {
					alert("Identifiants invalides");
				}
			}
		});
	} else {
		alert('Remplissez tous les champs');
		return false;
	}
}
