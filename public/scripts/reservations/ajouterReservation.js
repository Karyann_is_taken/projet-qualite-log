/*
* Ce script permet à un utlisateur connecté de créer une nouvelle réservation
*/

$(document).ready(function() {
  refresh();
});

function refresh() {
  sideMenuLogic();
}

$("#validerBoutton").on("click", ajouterReservation);
//Crée une réservation
function ajouterReservation() {
	event.preventDefault();

	let nbEmptyFields = 0;
	//Vérifie que tous les champs sont remplis
	$("#formReservation input").each(function (index, val) {
		if($(this).val() == '') {
			nbEmptyFields++;
		}
	});

	//Envoie la requete
	if (nbEmptyFields === 0) {
		var newReservation = {
			'material_id': $("#pGetID").text(),
			'user_id': sessionStorage.getItem("idUser"),
			'startDate': $("#inputDebut").val(),
			'endDate': $("#inputFin").val()
		};

		console.log(newReservation);

		$.post('/reservations/addReservation', newReservation, function(data) {
			alert("Réservation crée");
			$('#formReservation input').val('');
		});

	} else {
		alert('Remplissez tous les champs');
		return false;
	}
}
