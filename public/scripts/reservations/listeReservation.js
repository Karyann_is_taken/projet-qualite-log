/*
*	Ce script liste les réservations associées à un appareil
*/

$(document).ready(function() {
	refresh();
	//Ajute un évenement pour tous les liens cliquabes supprimer pour appeler la fonction supprimerReservation
	$('#tbodyReservations').on('click', 'td a.linkSupprimerReservation', supprimerReservation);
});

function refresh() {
	sideMenuLogic();
	getMateriel();
	populateTable();
}

//Récupère les informations liés à l'appareil
function getMateriel() {
	let url = '/materials/readMaterial/' + ($('#pGetID').text());
	$.getJSON(url, function (data) {
		$("h1").text("Tableau des réservations de l'appareil : " + data.name);
	});
}

//Remplit la table avec les réservations de l'appareil
function populateTable() {

	let tableContent = '';

	$.getJSON('/reservations/readReservation/' + $("#pGetID").text(), function(data) {
		$.each(data, function() {
			tableContent+='<tr>';
			tableContent+='<td>'+ this.user_id +'</td>';
			tableContent+='<td>'+ this.startDate +'</td>';
			tableContent+='<td>'+ this.endDate +'</td>';
			//Enregistre dans rel les informations requises pour la requête qui va supprimer une réservation
			tableContent+='<td><a href="#" class="linkSupprimerReservation" rel="'+this.user_id+ ' '+ this.startDate + ' ' + this.endDate +'">Supprimer</a></td>';
			tableContent += '</tr>';
		});
		$('#tbodyReservations').html(tableContent);
	});
}

//Supprime une réservation
function supprimerReservation() {
	//Récupère les informations dans l'attribut 'rel'
	let url = $(this).attr('rel');
	const args = url.split(' ');
	let dataDate = {
		"startDate": args[1],
		"endDate": args[2]
	};
	let confirmation = confirm("Voulez-vous vraiment supprimer cette réservation ?");
	if (confirmation) {
		$.ajax({
			type: 'DELETE',
			url: '/reservations/deleteReservation/' + args[0] + '/'+$("#pGetID").text(),
			data: dataDate
		}).done(function( response ) {
			refresh();
		});
	} else {
		return false;
	}
}
