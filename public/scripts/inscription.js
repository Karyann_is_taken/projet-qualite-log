/*
 * Ce script est pour inscrire un utilisateur
 */

$(document).ready(function() {
  refresh();
});

function refresh() {
  sideMenuLogic();
}

$("#validerBoutton").on("click", addUser);

//Ajoute un utilisateur
function addUser(event) {
	event.preventDefault();
	//Pour enlever les spans erreur si il y'en avait
	$(".error").remove();

	let nbEmptyFields = 0;

	//calcule le nimbres de champs vides
	$("#formUser input").each(function (index, val) {
		if($(this).val() == '') {
			nbEmptyFields++;
		}
	});
	//Validation regex e-mail
	var regEx = /^[^@\s]+@[^@\s]+\.[^@\s]+$/;
	var validEmail = regEx.test($('#inputMail').val());
	if (!validEmail) {
	    $('#inputMail').after('<span class="error">Entrez une adresse mail valide.</span>');
	}

	//validation regex matricule
	var regExMat = /^([a-zA-Z0-9_-]){7}$/;
	var validMatricule = regExMat.test($('#inputMatricule').val());
	if (!validMatricule) {
	    $('#inputMatricule').after('<span class="error">Entrez un matricule valide. (7 valeurs alpha-numériques)</span>');
	}

	//Si le formulaire est valide on crée l'objet et le post à /users/addUser
	if (nbEmptyFields === 0 && validEmail && validMatricule) {
		var newUser = {
			'first_name': $("#inputPrenom").val(),
			'last_name': $("#inputNom").val(),
			'mail_address': $("#inputMail").val(),
			'password': $("#motdepasse").val(),
			'registration_number': $("#inputMatricule").val(),
			'role': $("#selectRole").val()
		};

		console.log(newUser);

		$.post('/users/addUser', newUser, function(data) {
			alert("Utilisateur crée");
			$('#formUser input').val('');

		});

	} else {
		if (nbEmptyFields != 0) {
			alert('Remplissez tous les champs');
		}
		return false;
	}
}
