/*
 * Ce script gère la page permettant à un administrateur de modifier et supprimer un utilisateur
 */


$(document).ready(function() {
  refresh();
});

function refresh() {
  sideMenuLogic();
  getUtilisateur();
}

//récupère les informations d'un utilisateur et l'affiche dans les champs du formulaire
function getUtilisateur() {
	let url = '/users/readUser/' + ($('#pGetID').text());
	$.getJSON(url, function (data) {
		$("#inputNom").val(data.last_name);
		$("#inputPrenom").val(data.first_name);
		$("#inputMail").val(data.mail_address);
		$("#inputMatricule").val(data.registration_number);
		$("#selectRole").val(data.role);
	});
}

$("#validerBoutton").on("click", modifierUser);

//Modifie un utilisateur
function modifierUser() {
	event.preventDefault();
	$(".error").remove();

	let nbEmptyFields = 0;

	//Vérifiacation que les champs ne sont pas vides
	$("#formUser input").each(function (index, val) {
		if($(this).val() == '') {
			nbEmptyFields++;
		}
	});

	//Validation des champs par les regex
	var regEx = /^[^@\s]+@[^@\s]+\.[^@\s]+$/;
	var validEmail = regEx.test($('#inputMail').val());
	if (!validEmail) {
		$('#inputMail').after('<span class="error">Entrez une adresse mail valide.</span>');
	}

	var regExMat = /^([a-zA-Z0-9_-]){7}$/;
	var validMatricule = regExMat.test($('#inputMatricule').val());
	if (!validMatricule) {
		$('#inputMatricule').after('<span class="error">Entrez un matricule valide. (7 valeurs alpha-numériques)</span>');
	}

	//Requête pour modifier un utilisateur
	if (nbEmptyFields === 0 && validEmail && validMatricule) {
		var newUser = {
			'first_name': $("#inputPrenom").val(),
			'last_name': $("#inputNom").val(),
			'mail_address': $("#inputMail").val(),
			'password': $("#motdepasse").val(),
			'registration_number': $("#inputMatricule").val(),
			'role': $("#selectRole").val()
		};

		console.log(newUser);
		let url = '/users/updateUser/'+$('#pGetID').text();

		$.post(url, newUser, function(data) {
			alert("Utilisateur modifié");
			//update avec le nouvel utilisateur
			getUtilisateur();
			$("#motdepasse").val('');
		});

	} else {
		if (nbEmptyFields != 0) {
			alert('Remplissez tous les champs');
		}
		return false;
	}
}

$("#delBouton").on("click", supprimerUtilisateur);

//Supprime un utlisateur
function supprimerUtilisateur() {
	if (sessionStorage.getItem("idUser") == $('#pGetID').text()) {
		alert("Vous ne pouvez pas vous supprimer vous-même !");
	} else {
		let confirmation = confirm("Voulez-vous vraiment supprimer cet utilisateur ?");
		if (confirmation) {
			$.ajax({
				type: 'DELETE',
				url: '/users/deleteUser/'+$('#pGetID').text()
			}).done(function( response ) {
				//Retour à la liste des utilisateurs
				window.location.replace("/listeUtilisateur");
			});
		} else {
			return false;
		}
	}
}
