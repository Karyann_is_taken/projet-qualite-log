/*
 *	Ce script gère la page qui affiche la liste des utilisateurs et permet à un administrateur de valider l'emprunt d'un appareil par un des utilisateurs dans la liste
*/

$(document).ready(function() {
	refresh();
});

function refresh() {
	sideMenuLogic();
	getMateriel();
	populateTable();
}

//Récupère un matériel et affiche ses informations
function getMateriel() {
	let url = '/materials/readMaterial/' + ($('#pGetID').text());
	$.getJSON(url, function (data) {
		$("h1").text("Valider l'emprunt du matériel : " + data.name + " par un utilisateur");
		$("#pMateriel").text("Cliquer sur un utilisateur permet de valider l'emprunt de l'appareil " + data.name + " pour l'utilisateur choisi.");
	});
}

//Récupère la liste d'utilisateurs et l'insère dans le tableau
function populateTable() {

	let tableContent = '';

	$.getJSON('/users/readUsers', function(data) {
		$.each(data, function(data) {
			tableContent+='<tr class=\'clickable-row\'>';
			tableContent+='<td hidden>'+ this._id +'</td>';
			tableContent+='<td>'+ this.last_name +'</td>';
			tableContent+='<td>'+ this.first_name +'</td>';
			tableContent+='<td>'+ this.mail_address +'</td>';
			tableContent+='<td>'+ this.registration_number +'</td>';
			tableContent+='<td>'+ this.role +'</td>';
			tableContent += '</tr>';
		});
		$('#tbodyUsers').html(tableContent);
		//Chaque ligne va appeler la fonction avec les informations liées à la ligne insérée
		$(".clickable-row").click(function() {
	        addBorow($(this));
	    });
	});
}

//Valide qu'un utilisateur a emprunté un matériel
function addBorow(user) {
	let confirmation = confirm("Valider l'emprunt de l'appareil par " + user.children().eq(2).text() + " " + user.children().eq(1).text() + "?");
	if (confirmation) {
		var newEmprunt = {
			'material_id': $("#pGetID").text(),
			'user_id': user.children().eq(0).text()
		};

		$.post('/borrows/borrowMaterial', newEmprunt, function(data) {
			alert("Emprunt validé");
			//Retour à l'affichage de la page lié au matériel emprunté
			window.location.replace("/materiel/"+$("#pGetID").text());
		});
	} else {
		return false;
	}
}
