/*
 *	Ce script gère la page qui affiche la liste des utilisateurs et permet à un administrateur d'accéder aux détails d'un utilisateur en cliquant sur la ligne du tableau correspondante
*/

$(document).ready(function() {
	refresh();
});

function refresh() {
	sideMenuLogic();
	populateTable();
}

//Remplit la table des utilisateurs avec les informations en BDD
function populateTable() {

	let tableContent = '';
	//Récupère tous les utilisateurs et itère
	$.getJSON('/users/readUsers', function(data) {
		$.each(data, function(data) {
			//Ajoute une ligne au tableau
			tableContent+='<tr class=\'clickable-row\' data-href=\'/utilisateur/'+ this._id +'/modifier\'>';
			tableContent+='<td>'+ this.last_name +'</td>';
			tableContent+='<td>'+ this.first_name +'</td>';
			tableContent+='<td>'+ this.mail_address +'</td>';
			tableContent+='<td>'+ this.registration_number +'</td>';
			tableContent+='<td>'+ this.role +'</td>';
			tableContent += '</tr>';
		});
		$('#tbodyUsers').html(tableContent);
		//Rend toute la ligne un lien cliquable vers l'utilisateur cliqué
		$(".clickable-row").click(function() {
	        window.location = $(this).data("href");
	    });
	});
}
