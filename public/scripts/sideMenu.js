/*
 * Ce script récu^ère la session et gère l'affichage des éléments dans le menu de navigation en fonction des informations de sessions
 */


//En fonction de si l'utilisateur est connecté et son rôle affiche différents éléments dans le menu de navigation
function sideMenuLogic() {
	getSession();
	$("#sideMenuDeconnexion").hide();
	$("#sideMenuInscription").hide();
	$("#listeUtilisateur").hide();


	var idUser = sessionStorage.getItem("idUser");
	//console.log(idUser);

	if(sessionStorage.getItem("idUser") === "undefined" || sessionStorage.getItem("idUser") === null) {

	} else {
		$("#sideMenuConnexion").hide();
		$("#sideMenuDeconnexion").show();
		if(sessionStorage.getItem("role") === "administrator") {
			$("#sideMenuInscription").show();
			$("#listeUtilisateur").show();
		}
	}

}

//Récupère les informations de sessions et les stocke
function getSession() {

	if(sessionStorage.getItem("idUser") === "undefined" ||  sessionStorage.getItem("idUser") === null) {
		console.log("GET /session");
		$.ajax({
			type: "GET",
			url: '/session',
			async : false,
			success: function(data) {
				sessionStorage.setItem("idUser", data.idUser);
				sessionStorage.setItem("first_name", data.first_name);
				sessionStorage.setItem("last_name", data.last_name);
				sessionStorage.setItem("mail_address", data.mail_address);
				sessionStorage.setItem("role", data.role);
			},
			error: function (dataError) {
				console.log(dataError);
			}
		});
	}
}

//Deconnecte l'utilisateur et détruit la session
$("#sideMenuDeconnexion").click(function () {
	sessionStorage.clear();
	$.get('/deconnexion');
	alert("Vous êtes déconnecté");
	document.location.href = '/';
});
