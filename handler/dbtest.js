const database = "ProjetQualiteLogicielle_Tests";
const url = "mongodb://localhost:27017/" + database;

const mongoose = require("mongoose");
const connectionOptions = {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
};
mongoose
    .connect(
        process.env.MONGODB_URI || url,
        connectionOptions
    )
    .then(
        console.log("[mongoose] database connecting to " + database + " ...")
    );

var conn = mongoose.connection;
conn.once('open', () => console.log("[mongoose] database connected to " + database + "."));
conn.on('error', console.error.bind(console, "MongoDB connection to " + url + " error:"));

mongoose.Promise = global.Promise;

module.exports = {
    conn,
    User: require("../models/user"),
    Borrow: require("../models/borrow"),
    Material: require("../models/material"),
    Notification: require("../models/notification"),
    Reservation: require("../models/reservation"),
};
