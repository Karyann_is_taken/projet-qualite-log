var express = require('express');
var router = express.Router();
var userController = require('../controllers/user.controller');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Accueil' });
});

router.get('/inscription', function (req, res) {
	var session = req.session;
	if(session.role === "administrator") {
		res.render('utilisateur/inscription', { title: 'Inscription' });
	} else {
		res.render('refuse', {ressource: 'Inscription', cause: "Vous n'êtes pas connecté en tant qu'administrateur."});
	}
});

router.get('/connexion', function (req, res) {
	var session = req.session;
	if(!session.idUser) {
		res.render('utilisateur/connexion', { title: 'Connexion' });
	} else {
		res.render('refuse', {ressource: 'Connexion', cause: 'Cause : Vous êtes déjà connecté.'});
	}
});

router.post('/identification', async function (req, res) {
  var session = req.session;
  userController.confirmIdentity(req, res, req.body.mail_address, req.body.password)
  .then((result) =>{
    if(result && !result.error) {
      session.idUser = result._id;
      session.first_name = result.first_name;
      session.last_name = result.last_name;
      session.mail_address = result.mail_address;
      session.role = result.role;
      res.end('succes');
    }else{
      res.end('failed');
    }

});


});

router.get('/session', function (req, res) {
	res.send(req.session);
});

router.get('/deconnexion', function (req, res) {
	req.session.destroy();
});

router.get('/listeUtilisateur', function (req, res) {
  var session = req.session;
  if(session.role === "administrator") {
	  res.render('utilisateur/listeUtilisateur', { title: 'Liste des utilisateurs', user: req.session });
  } else {
	  res.render('refuse', {ressource: 'Liste utilisateurs', cause: "Vous n'êtes pas connecté en tant qu'administrateur."});
  }
});

router.get('/utilisateur/:idUser/modifier', function (req, res) {
	var session = req.session;
	if(session.role === "administrator") {
		res.render('utilisateur/modifierUtilisateur', { title: req.params.idUser });
	} else {
		res.render('refuse', {ressource: 'Modifier un utilisateur', cause: "Vous n'êtes pas connecté en tant qu'administrateur."});
	}
});

router.get('/listeMateriel', function (req, res) {
  res.render('materiel/listeMateriel', { title: 'Liste du matériel' });
});

router.get('/materiel/creerMateriel', function (req, res) {
  var session = req.session;
  if(session.role === "administrator") {
	  res.render('materiel/creerMateriel', { title: 'Créer un nouveau matériel' });
  } else {
	  res.render('refuse', {ressource: 'Créer un matériel', cause: "Vous n'êtes pas connecté en tant qu'administrateur."});
  }
});

router.get('/materiel/:idMateriel', function (req, res) {
  res.render('materiel/materiel', { idMateriel: req.params.idMateriel });
});

router.get('/materiel/:idMateriel/modifierMateriel', function (req, res) {
  var session = req.session;
  if(session.role === "administrator") {
	res.render('materiel/modifierMateriel', { idMateriel: req.params.idMateriel });
  } else {
	res.render('refuse', {ressource: 'Créer un matériel', cause: "Vous n'êtes pas connecté en tant qu'administrateur."});
  }
});

router.get('/materiel/:idMateriel/emprunt', function (req, res) {
  var session = req.session;
  if(session.role === "administrator") {
  	res.render('emprunt', { idMateriel: req.params.idMateriel });
  } else {
  	res.render('refuse', {ressource: 'Valider l\'emprunt d\'un matériel par un utilisateur', cause: "Vous n'êtes pas connecté en tant qu'administrateur."});
  }
});

router.get('/materiel/:idMateriel/reservation', function (req, res) {
  var session = req.session;
  if(!session.idUser) {
	res.render('refuse', {ressource: 'Réserver un matériel', cause: 'Cause : Vous n\'êtes pas connecté.'});
  } else {
	res.render('reservations', { idMateriel: req.params.idMateriel });
  }
});

router.get('/materiel/:idMateriel/reservation/creerReservation', function (req, res) {
  var session = req.session;
  if(!session.idUser) {
	  res.render('refuse', {ressource: 'Créer une réservation', cause: 'Cause : Vous n\'êtes pas connecté.'});
  } else {
	  res.render('creerReservation', { idMateriel: req.params.idMateriel });
  }

});

module.exports = router;
//Database
