var express = require('express');
var router = express.Router();
var userController = require('../controllers/user.controller');
var User = require('../models/user');

var userMongoMock = require('../mocks/mongo/user.json');

//const User = userModel.User;
/* GET users listing. */
router.post('/addUser', async function (req, res) {
  res.send(await userController.createUser(req, res));
}
);

router.get('/readUsers', async function (req, res) {
  res.send(await userController.readAllUser(req, res));
}
);

router.get('/readUser/:idUser', async function (req, res) {
  res.send(await userController.readUser(req, res, req.params.idUser));
}
);

router.post('/updateUser/:idUser', async function (req, res) {
  res.send(await userController.updateUser(req, res));
}
);

router.delete('/deleteUser/:idUser', async function (req, res) {
  userController.deleteUser(req, res, req.params.idUser);
}
);

module.exports = router;
