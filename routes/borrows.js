var express = require('express');
var router = express.Router();
var borrowController = require('../controllers/borrow.controller');
var Borrow = require('../models/borrow');

// var borrowMongoMock = require('../mocks/mongo/borrow.json'); //TODO

router.post('/addBorrow', (req, res) => {
  // req = materialMongoMock;
  borrowController.createBorrow(req, res);
}
);

router.get('/readBorrows', (req, res) => {
  borrowController.readAllBorrow(req, res);
}
);

router.get('/readBorrow/:idUser/:idMaterial', (req, res) => {
  borrowController.readBorrow(req, res);
}
);

router.post('/updateBorrow', async function(req, res){
  res.send(await borrowController.updateBorrow(req, res));
}
);

router.delete('/deleteBorrow/:idUser/:idMaterial', async function(req, res){
  res.send(await borrowController.deleteBorrow(req, res));
}
);

router.post('/borrowMaterial', (req, res) => {
  borrowController.borrowingOfMaterial(req, res, req.body.material_id, req.body.user_id);
});

router.post('/returnMaterial', (req, res) => {
  borrowController.returnMaterial(req, res, req.body.material_id);
});

module.exports = router;
