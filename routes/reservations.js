var express = require('express');
var router = express.Router();
var reservationController = require('../controllers/reservation.controller');
var Reservation = require('../models/reservation');

// var reservationMongoMock = require('../mocks/mongo/reservation.json'); //TODO

router.post('/addReservation', (req, res) => {
  // req = materialMongoMock;
reservationController.createReservation(req, res);
}
);

router.get('/readReservations', async function (req, res) {
  res.send(await reservationController.readAllReservation(req, res));
}
);

router.get('/readReservation/:idMaterial', async function (req, res) {
  res.send(await reservationController.getReservationsOfMaterial(req, res));
}
);

router.post('/updateReservation/:idUser/:idMaterial', async function (req, res) {
  res.send(await reservationController.updateReservation(req, res));
}
);

router.delete('/deleteReservation/:idUser/:idMaterial', async function (req, res) {
  res.send(await reservationController.deleteReservation(req, res));
}
);

module.exports = router;
