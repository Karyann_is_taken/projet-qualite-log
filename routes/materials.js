var express = require('express');
var router = express.Router();
var materialController = require('../controllers/material.controller');
var Material = require('../models/material');

var materialMongoMock = require('../mocks/mongo/material.json');

router.post('/addMaterial', async function (req, res) {
  // req = materialMongoMock;
  res.send(await materialController.createMaterial(req, res));
}
);

router.get('/readMaterials', async function (req, res) {
  res.send(await materialController.readAllMaterial(req, res));
}
);

router.get('/readMaterial/:idMaterial', async function (req, res) {
  res.send(await materialController.readMaterial(req, res, req.params.idMaterial));
}
);

router.post('/updateMaterial/:idMaterial', async function (req, res) {
  res.send(await materialController.updateMaterial(req, res));
}
);

router.delete('/deleteMaterial/:idMaterial', async function (req, res) {
  res.send(await materialController.deleteMaterial(req, res, req.params.idMaterial));
}
);

module.exports = router;
