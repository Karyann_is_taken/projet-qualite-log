const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

const notificationSchema = new Schema({
  material_id: {
    type: ObjectId,
    required: true
  },
  user_id: {
    type: ObjectId,
    required: true
  },
  notifDate: {
    type: Date,
    required: true
  },
  notifType: {
    type: String,
    required: true,
    regex: "/^(borrowed | disponible)$/"
  }
}
);

const Notification = mongoose.model('Notification', notificationSchema);
module.exports = Notification;
