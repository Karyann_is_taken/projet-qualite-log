const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  last_name: {
    type: String,
    required: true,
    maxLength: 30
  },
  first_name: {
    type: String,
    required: true,
    maxLength: 30
  },
  mail_address: {
    type: String,
    required: true,
    maxLength: 100,
    regex: "/^[^@\s]+@[^@\s]+\.[^@\s]+$/"
  },
  registration_number: {
    type: String,
    required: true,
    regex: "/^([a-zA-Z0-9_-]){7}$/"
  },
  password: {
    type: String,
    required: true,
    maxLength: 32
  },
  role: {
    type: String,
    required: true,
    regex: "/^(administrator|borrower)$/"
  }
});

const User = mongoose.model('User', userSchema);
module.exports = User;
