const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const materialSchema = new Schema({
  name: {
    type: String,
    required: true,
    maxLength: 30
  },
  version: {
    type: String,
    required: true,
    maxLength: 15,
    regex: "/^([a-zA-Z0-9_-]){3,15}$/"
  },
  reference: {
    type: String,
    required: true,
    regex: "/^(AN|AP|XX)\d\d\d$/"
  },
  photo: {
    type: String
  },
  tel_number: {
    type: String,
    regex: "/^([0-9]){10}$/"
  },
  borrowed: {
    type: Boolean
  }
});

const Material = mongoose.model('Material', materialSchema);
module.exports = Material;
