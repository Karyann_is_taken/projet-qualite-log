const chai = require("chai");
const expect = chai.expect;
const db = require("../handler/dbtest");
const Material = db.Material;
const materialController = require("../controllers/material.controller");
const mongoMock = require("../mocks/mongo/mockMaterial");


// -----------------------------------------------------------------------------

describe("# UT-Material", function () {

    beforeEach((done) => {
        db.conn.collections.materials.drop(() => {
            // console.log("[db] collection materials dropped");
            done();
        });
    });

    afterEach((done) => {
        db.conn.collections.materials.drop(() => {
            // console.log("[db] collection materials dropped");
            done();
        });
    });

    // -----------------------------------------------------------------------------

    describe("Fct: createMaterial", function () {

        it("should not create material for invalid material's informations", async function () {
            const m = mongoMock.invalidMaterial1();
            req = { body: m };

            await materialController.createMaterial(req, null)
                .then((successCallback, failureCallback) => {
                    expect(failureCallback).to.be.undefined;
                    expect(successCallback).to.include({ error: "badRequest" });
                });
        });

        it("should create the material", async function () {
            const m = mongoMock.material2();
            req = { body: m };

            await materialController.createMaterial(req, null)
                .then((successCallback, failureCallback) => {
                    expect(failureCallback).to.be.undefined;
                    expect(successCallback).to.include(m);
                });

            await Material.find(m)
                .then((success, err) => {
                    expect(success).to.have.lengthOf(1);
                });
        });

        it("should not create an already existing material ", async function () {
            const m = mongoMock.material1();
            const mat = new Material(m);
            var matInfo = await mat.save().catch((err) => { console.log("**m insert err", err) })

            req = { body: m };

            await materialController.createMaterial(req, null)
                .then((successCallback, failureCallback) => {
                    expect(failureCallback).to.be.undefined;
                    expect(successCallback).to.include(m);
                });

            await Material.find(m)
                .then((success, err) => {
                    expect(success).to.have.lengthOf(1);
                });
        });
    });

    // ----------------------------------------------------------------------------------------------

    describe("Fct: updateMaterial", function () {
        it("should update a material", async function () {
            const m1 = mongoMock.material1();
            const m2 = mongoMock.material2();
            const mat1 = new Material(m1);
            var idM1 = await mat1.save().catch((err) => { console.log("**m1 insert err", err) })

            req = {
                params: { idMaterial: idM1._id },
                body: m2
            };

            await materialController.updateMaterial(req, null)
                .then((successCallback, failureCallback) => {
                    expect(failureCallback).to.be.undefined;
                    expect(successCallback).to.include(m1);
                });

            await Material.findById(idM1._id)
                .then((success, err) => {
                    expect(success).to.include(m2);
                });
        });

        it("should not update an inexisting material", async function () {
            const m1 = mongoMock.material1();
            const m2 = mongoMock.material2();
            const mat1 = new Material(m1);
            var idM1 = await mat1.save().catch((err) => { console.log("**m1 insert err", err) })
            await Material.deleteOne({ _id: idM1._id });

            req = {
                params: { idMaterial: idM1._id },
                body: m2
            };

            await materialController.updateMaterial(req, null)
                .then((successCallback, failureCallback) => {
                    expect(failureCallback).to.be.undefined;
                    expect(successCallback).to.be.null;
                });

            await Material.findById(idM1._id)
                .then((success, err) => {
                    expect(success).to.be.null;
                });
        });

    });

    // ----------------------------------------------------------------------------------------------

    describe("Fct: readMaterial", function () {
        it("should return material's informations by its id", async function () {
            const m = mongoMock.material1();
            const mat = new Material(m);
            const matInfo = await mat.save().catch((err) => { console.log("**m insert err", err) });

            await materialController.readMaterial(null, null, matInfo._id)
                .then((successCallback, failureCallback) => {
                    expect(failureCallback).to.be.undefined;
                    expect(successCallback).to.include(m);
                });
        });

        it("should not get infos from an inexisting material", async function () {
            const u = new Material(mongoMock.material1());
            const matInfo = await u.save().catch((err) => { console.log("**u insert err", err) });

            await Material.deleteOne({ _id: matInfo._id });

            await materialController.readMaterial(null, null, matInfo._id)
                .then((successCallback, failureCallback) => {
                    expect(failureCallback).to.be.undefined;
                    expect(successCallback).to.be.null;
                });
        });
    });

    // ----------------------------------------------------------------------------------------------

    describe("Fct: readAllMaterial", function () {
        it("should return all material id", async function () {
            const m1 = mongoMock.material1();
            const m2 = mongoMock.material2();
            const mat1 = new Material(m1);
            const mat2 = new Material(m2);
            var idM1 = await mat1.save().catch((err) => { console.log("**m1 insert err", err) })
            var idM2 = await mat2.save().catch((err) => { console.log("**m2 insert err", err) })

            await materialController.readAllMaterial(null, null)
                .then((successCallback, failureCallback) => {
                    expect(failureCallback).to.be.undefined;
                    expect(successCallback[0]).to.include(m1);
                    expect(successCallback[1]).to.include(m2);
                });
        });

        it("should not return deleted materials", async function () {
            const m1 = mongoMock.material1();
            const m2 = mongoMock.material2();
            const mat1 = new Material(m1);
            const mat2 = new Material(m2);
            var idM1 = await mat1.save().catch((err) => { console.log("**m1 insert err", err) })
            var idM2 = await mat2.save().catch((err) => { console.log("**m2 insert err", err) })
            await Material.deleteOne({ _id: idM2._id });

            await materialController.readAllMaterial(null, null)
                .then((successCallback, failureCallback) => {
                    expect(failureCallback).to.be.undefined;
                    expect(successCallback).to.have.lengthOf(1);
                    expect(successCallback[0]).to.include(m1);
                });
        });
    });

    // ----------------------------------------------------------------------------------------------

    describe("Fct: changeBorrowedStatus", function () {
        it.skip("should change borrow status of material", async function () {
            const m = mongoMock.material1();
            const mat = new Material(m);
            const idMat = await mat.save().catch((err) => { console.log("**m insert err", err) })
            // const myBorrowed = true;

            await materialController.changeBorrowedStatus(null, null, idMat._id, m.borrowed)
                .then((successCallback, failureCallback) => {
                    console.log("--s", successCallback);
                    console.log("--f", failureCallback);
                    expect(failureCallback).to.be.undefined;
                });
        });

        it.skip("should not change borrow status of inexisting material", async function () {
            // TODO
        });
    });

    // ----------------------------------------------------------------------------------------------

    describe("Fct: deleteMaterial", function () {
        it.skip("should delete an existing material", async function () {
            // TODO
        });

        it.skip("should not delete an inexisting material", async function () {
            // TODO
        });
    });
});