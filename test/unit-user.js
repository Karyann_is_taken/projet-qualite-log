const chai = require("chai");
const expect = chai.expect;
const db = require("../handler/dbtest");
const User = db.User;
const userController = require("../controllers/user.controller");
const mongoMock = require("../mocks/mongo/mockUser");


// ----------------------------------------------------------------------------------------------

describe("# UT-Utilisateurs", function () {
  // Ensemble des méthodes de tests pour les fonction utilisateurs

  beforeEach((done) => {
    db.conn.collections.users.drop(() => {
      // console.log("[db] collection users dropped");
      done();
    });
  });

  afterEach((done) => {
    db.conn.collections.users.drop(() => {
      // console.log("[db] collection users dropped");
      done();
    });
  });

  // -----------------------------------------------------------------------------------------------
  describe("Model:", function () {
    it.skip("should create the user", function () {
      user = new User({
        "last_name": "Chirac",
        "first_name": "Jacques",
        "mail_address": "test@test.test",
        "registration_number": "numero0",
        "password": "motdepasse",
        "role": "administrator"
      });
      return expect(user.validate()).to.be.fulfilled;
    });

    it.skip("should NOT create the user", function () {
      user = new User({
        "first_name": "Jacques",
        "mail_address": "test@test.test",
        "registration_number": "numero0",
        "password": "motdepasse",
        "role": "administrator"
      });
      return expect(user.validate()).to.be.rejected;
    });
  });

  // -----------------------------------------------------------------------------------------------
  describe("Fct: createUser", function () {

    it("should not create user for invalid user's informations", async function () {
      const u = mongoMock.invalidUser();
      req = { body: u };

      await userController.createUser(req, null)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.include({ error: "badRequest" });
        });

    });

    it("should create the user", async function () {
      const u = mongoMock.validUser();
      req = { body: u };

      await userController.createUser(req, null)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.include(u);
        });

      await User.find(u)
        .then((success, err) => {
          expect(success).to.have.lengthOf(1);
        });
    });

    it("should not create an already existing user ", async function () {
      const u = mongoMock.validUser();
      const user = new User(u);
      var userInfo = await user.save().catch((err) => { console.log("**u insert err", err) })

      req = { body: u };

      await userController.createUser(req, null)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.include(u);
        });

      await User.find(u)
        .then((success, err) => {
          expect(success).to.have.lengthOf(1);
        });
    });
  });

  // -----------------------------------------------------------------------------------------------
  describe("Fct: updateUser", function () {
    it("should update a user", async function () {
      const u1 = mongoMock.validUser();
      const u2 = mongoMock.validUser2();
      const user1 = new User(u1);
      var idU1 = await user1.save().catch((err) => { console.log("**u1 insert err", err) })

      req = {
        params: { idUser: idU1._id },
        body: u2
      };

      await userController.updateUser(req, null)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.include(u1);
        });

      await User.findById(idU1._id)
        .then((success, err) => {
          expect(success).to.include(u2);
        });
    });

    it("should not update an inexisting user", async function () {
      const u1 = mongoMock.validUser();
      const u2 = mongoMock.validUser2();
      const user1 = new User(u1);
      var idU1 = await user1.save().catch((err) => { console.log("**u1 insert err", err) })
      await User.deleteOne({ _id: idU1._id });

      req = {
        params: { idUser: idU1._id },
        body: u2
      };

      await userController.updateUser(req, null)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.be.null;
        });

      await User.findById(idU1._id)
        .then((success, err) => {
          expect(success).to.be.null;
        });

    });

  });
  // -----------------------------------------------------------------------------------------------
  describe("Fct: readUser", function () {
    it("should return user's informations by its id", async function () {
      ///ajoute 1 user dans la db
      const u = mongoMock.validUser();
      const user = new User(u);
      const userInfo = await user.save().catch((err) => { console.log("**u insert err", err) });

      //test de la fct readUser
      await userController.readUser(null, null, userInfo._id)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.include(u);
        });
    });

    it("should not get infos from an inexisting user", async function () {
      // creer un user pour avoir un id
      const u = new User(mongoMock.validUser());
      const userInfo = await u.save().catch((err) => { console.log("**u insert err", err) });
      // supprimer user pour ne pas le trouver
      await User.deleteOne({ _id: userInfo._id });

      // test de la fct readUser avec l'id de l'user supprimé
      await userController.readUser(null, null, userInfo._id)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.be.null;
        });
    });
  });

  // -----------------------------------------------------------------------------------------------
  describe("Fct: readAllUser", function () {
    it("should return all users", async function () {

      const u1 = mongoMock.validUser();
      const u2 = mongoMock.validUser2();
      const user1 = new User(u1);
      const user2 = new User(u2);
      var idU1 = await user1.save().catch((err) => { console.log("**u1 insert err", err) })
      var idU2 = await user2.save().catch((err) => { console.log("**u2 insert err", err) })

      await userController.readAllUser(null, null)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback[0]).to.include(u1);
          expect(successCallback[1]).to.include(u2);
        });
    });

    it("should not return deleted users", async function () {
      const u1 = mongoMock.validUser();
      const u2 = mongoMock.validUser2();
      const user1 = new User(u1);
      const user2 = new User(u2);
      var idU1 = await user1.save().catch((err) => { console.log("**u1 insert err", err) })
      var idU2 = await user2.save().catch((err) => { console.log("**u2 insert err", err) })
      await User.deleteOne({ _id: idU2._id });

      await userController.readAllUser(null, null)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.have.lengthOf(1);
          expect(successCallback[0]).to.include(u1);
        });
    });
  });
  // -----------------------------------------------------------------------------------------------
  describe("Fct: getAllAdmins", function () {
    it("should return all admin user", async function () {
      const u1 = mongoMock.validUser();
      const u2 = mongoMock.adminUser();
      const user1 = new User(u1);
      const user2 = new User(u2);
      var idU1 = await user1.save().catch((err) => { console.log("**u1 insert err", err) })
      var idU2 = await user2.save().catch((err) => { console.log("**u2 insert err", err) })

      await userController.getAllAdmins(null, null)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.have.lengthOf(1);
          expect(successCallback[0]).to.include(u2);
        });
    });

    it("should not return deleted admin", async function () {
      const u1 = mongoMock.adminUser();
      const u2 = mongoMock.adminUser2();
      const user1 = new User(u1);
      const user2 = new User(u2);
      var idU1 = await user1.save().catch((err) => { console.log("**u1 insert err", err) })
      var idU2 = await user2.save().catch((err) => { console.log("**u2 insert err", err) })
      await User.deleteOne({ _id: idU1._id });

      await userController.getAllAdmins(null, null)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.have.lengthOf(1);
          expect(successCallback[0]).to.include(u2);
        });
    });
  });
  // --------------------------------------------------------------------------------------------
  describe("Fct: confirmIdentity", function () {
    it("should confirm that user exist with those logins", async function () {
      const u = mongoMock.validUser();
      const user = new User(u);
      const userInfo = await user.save().catch((err) => { console.log("**u insert err", err) });

      await userController.confirmIdentity(null, null, u.mail_address, u.password)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.include(u);
        });

    });

    it("should not confirm unregistered user", async function () {
      const u1 = mongoMock.validUser();
      const u2 = mongoMock.validUser2();
      const user1 = new User(u1);
      const userInfo = await user1.save().catch((err) => { console.log("**u insert err", err) });

      await userController.confirmIdentity(null, null, u2.mail_address, u2.password)
        .then((successCallback, failureCallback) => {
          expect(failureCallback).to.be.undefined;
          expect(successCallback).to.be.null;
        });
    });

  });
  // --------------------------------------------------------------------------------------------
  describe("Fct: deleteUser", function () {
    it.skip("should delete an existing user", async function () {
      // TODO

    });

    it.skip("should not delete an inexisting user", async function () {
      // TODO
    });

    it.skip("should notify on delete", async function () {
      // TODO
    });

    it.skip("should not notify when delete error", async function () {
      // TODO
    });
  });



});
