const chai = require("chai");
const expect = chai.expect;
const db = require("../handler/dbtest");
const Reservation = db.Reservation;
const reservationController = require("../controllers/reservation.controller");
const mongoMock = require("../mocks/mongo/mockReservation");


// -----------------------------------------------------------------------------

describe("# UT-Reservation", function () {

    beforeEach((done) => {
        db.conn.collections.reservations.drop(() => {
            // console.log("[db] collection reservations dropped");
            done();
        });
    });

    afterEach((done) => {
        db.conn.collections.reservations.drop(() => {
            // console.log("[db] collection reservations dropped");
            done();
        });
    });

    // -----------------------------------------------------------------------------

    describe("Fct: createReservation", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: readReservation", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: updateReservation", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: deleteReservation", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: getReservationsOfUser", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: getReservationsOfMaterial", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: getReservationsOfMaterialBetweenDates", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: getReservationsOfMaterialAfterDate", function () {

    });

});