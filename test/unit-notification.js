const chai = require("chai");
const expect = chai.expect;
const db = require("../handler/dbtest");
const Notification = db.Notification;
const notificationController = require("../controllers/notification.controller");
// const mongoMock = require("../mocks/mongo/mockNotification");


// -----------------------------------------------------------------------------

describe("# UT-Notification", function () {

    beforeEach((done) => {
        db.conn.collections.notifications.drop(() => {
            // console.log("[db] collection notifications dropped");
            done();
        });
    });

    afterEach((done) => {
        db.conn.collections.notifications.drop(() => {
            // console.log("[db] collection notifications dropped");
            done();
        });
    });

    // -----------------------------------------------------------------------------

    describe("Fct: createNotification", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: deleteNotification", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: getNotificationsOfUser", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: makeBorrowedNotification", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: makeDisponibilityNotification", function () {

    });

});