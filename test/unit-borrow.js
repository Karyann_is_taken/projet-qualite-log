const chai = require("chai");
const expect = chai.expect;
const db = require("../handler/dbtest");
const Borrow = db.Borrow;
const borrowController = require("../controllers/borrow.controller");
// const mongoMock = require("../mocks/mongo/mockBorrow");


// -----------------------------------------------------------------------------

describe("# UT-Borrow", function () {

    beforeEach((done) => {
        db.conn.collections.borrows.drop(() => {
            // console.log("[db] collection borrows dropped");
            done();
        });
    });

    afterEach((done) => {
        db.conn.collections.borrows.drop(() => {
            // console.log("[db] collection borrows dropped");
            done();
        });
    });

    // -----------------------------------------------------------------------------

    describe("Fct: createBorrow", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: readBorrow", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: readAllBorrow", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: updateBorrow", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: deleteBorrow", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: getBorrowsOfUser", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: getBorrowsOfMaterial", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: getCurrentBorrower", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: borrowingOfMaterial", function () {

    });

    // -----------------------------------------------------------------------------

    describe("Fct: returnMaterial", function () {

    });


});