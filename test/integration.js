const sinon = require("sinon");
const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
chai.use(chaiHttp);
const app = require("../app");
chai.use(require("chai-as-promised"));
const db = require("../handler/db");
const User = db.User;
const Material = db.Material;

const mongoMockUser = require("../mocks/mongo/mockUser");
const mongoMockMaterial = require("../mocks/mongo/mockMaterial");
/*******************
*	      USER       *
*******************/

describe("User :", function () {
	let userId;
	it("should create the user", (done) => {
		chai
			.request(app)
			.post("/users/addUser")
			.set("content-type", "application/json")
			.send({
				"last_name": "Chirac",
				"first_name": "Jacques",
				"mail_address": "test@test.test",
				"registration_number": "numero0",
				"password": "motdepasse",
				"role": "administrator"
			})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				userId = res.body._id;
				done();
			});
	});

	it("should read the user", (done) => {
		chai
			.request(app)
			.get("/users/readUser/" + userId)
			.set("content-type", "application/json")
			.send({})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	it("should read the users", (done) => {
		chai
			.request(app)
			.get("/users/readUsers")
			.set("content-type", "application/json")
			// .send({})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	it("should update the user", (done) => {
		chai
			.request(app)
			.post("/users/updateUser/" + userId)
			.set("content-type", "application/json")
			.send({	"last_name": "Sarkozy",
							"first_name": "Nicolas",
							"mail_address": "test@test.test",
							"registration_number": "numero0",
							"password": "motdepasse",
							"role": "administrator"
						})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	it("should delete the user", (done) => {
		chai
			.request(app)
			.delete("/users/deleteUser/" + userId)
			.set("content-type", "application/json")
			// .send({})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});
});

/*******************
*	     MATERIAL    *
*******************/

describe("Material :", function () {
	let materialId;
	it("should create the material", (done) => {
		chai
			.request(app)
			.post("/materials/addMaterial")
			.set("content-type", "application/json")
			.send({
					    "name": "Android FG",
					    "version": "HELP",
					    "reference": "AN888",
					    "borrowed": false
						})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				materialId = res.body._id;
				done();
			});
	});

	it("should read the material", (done) => {
		chai
			.request(app)
			.get("/materials/readMaterial/" + materialId)
			.set("content-type", "application/json")
			// .send({})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	it("should read the materials", (done) => {
		chai
			.request(app)
			.get("/materials/readMaterials")
			.set("content-type", "application/json")
			// .send({})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	it("should update the material", (done) => {
		chai
			.request(app)
			.post("/materials/updateMaterial/" + materialId)
			.set("content-type", "application/json")
			.send({	"name": "iphone 16",
							"version": "12.2",
							"reference": "A234215",
							"photo": "labelfoto",
							"tel_number": "0606060606",
							"borrowed": false
						})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	it("should delete the material", (done) => {
		chai
			.request(app)
			.delete("/materials/deleteMaterial/" + materialId)
			.set("content-type", "application/json")
			// .send({})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});
});

/*******************
*	      BORROW     *
*******************/

describe("Borrow :", function () {

	const jsonMat = mongoMockMaterial.material1();
	const objMat = new Material(jsonMat);
	const jsonUser = mongoMockUser.validUser();
	const objUser = new User(jsonUser);
	const bddMat = objMat.save();
	const bddUser = objUser.save();

	const jsonMat2 = mongoMockMaterial.material2();
	const objMat2 = new Material(jsonMat2);
	const jsonUser2 = mongoMockUser.validUser2();
	const objUser2 = new User(jsonUser2);
	const bddMat2 = objMat2.save();
	const bddUser2 = objUser2.save();

	let userId = objUser._id;
	let materialId = objMat._id;
	let userId2 = objUser2._id;
	let materialId2 = objMat2._id;

	let startDate;
	it("should create the borrow", (done) => {
		chai
			.request(app)
			.post("/borrows/addBorrow")
			.set("content-type", "application/json")
			.send({
			        "material_id": materialId,
			        "user_id": userId
    			  })
			.end((err, res) => {
				expect(res.status).to.equal(200);
				startDate = res.body.startDate;
				done();
			});
	});

	let startDate2;
	it("should borrow the material", (done) => {
		chai
			.request(app)
			.post("/borrows/borrowMaterial")
			.set("content-type", "application/json")
			.send({
							"material_id": materialId2,
							"user_id": userId2
						})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				startDate2 = res.body.startDate;
				done();
			});
	});

	it("should read the borrows", (done) => {
		chai
			.request(app)
			.get("/borrows/readBorrows")
			.set("content-type", "application/json")
			.send({})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	it("should update the borrow", (done) => {
		chai
			.request(app)
			.post("/borrows/updateBorrow")
			.set("content-type", "application/json")
			.send({
			        "material_id": materialId,
			        "user_id": userId,
							"startDate": startDate,
							"endDate": "2025-01-07T19:18:18.044Z"
    			  })
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	// La route /borrows/returnMaterial ne renvoie rien
	// it("should return the borrowed material", (done) => {
	// 	chai
	// 		.request(app)
	// 		.post("/borrows/returnMaterial")
	// 		.set("content-type", "application/json")
	// 		.send({
	// 						"material_id": materialId2,
	// 					})
	// 		.end((err, res) => {
	// 			expect(res.status).to.equal(200);
	// 			endDate2 = res.body.endDate;
	// 			done();
	// 		});
	// });

	it("should read the borrow", (done) => {
		chai
			.request(app)
			.get("/borrows/readBorrow/" + userId + "/" + materialId)
			.set("content-type", "application/json")
			.send({"startDate": startDate})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	it("should delete the borrow", (done) => {
		chai
			.request(app)
			.delete("/borrows/deleteBorrow/" + userId + "/" + materialId)
			.set("content-type", "application/json")
			.send({
							"startDate": startDate
						})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});

	});
});

/*******************
*	   Reservation   *
*******************/

describe("Reservation :", function () {

	const jsonMat = mongoMockMaterial.material1();
	const objMat = new Material(jsonMat);
	const jsonUser = mongoMockUser.validUser();
	const objUser = new User(jsonUser);
	const bddMat = objMat.save();
	const bddUser = objUser.save();


	let userId = objUser._id;
	let materialId = objMat._id;

	let startDate = "2021-01-09T16:59:52.876Z";
	let endDate = "2023-01-09T16:59:52.876Z";

	it("should create the reservation", (done) => {
		chai
			.request(app)
			.post("/reservations/addReservation")
			.set("content-type", "application/json")
			.send({
			        "material_id": materialId,
			        "user_id": userId,
							"startDate": startDate,
							"endDate": endDate
    			  })
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	it("should read the reservation", (done) => {
		chai
			.request(app)
			.get("/reservations/readReservation/" + materialId)
			.set("content-type", "application/json")
			.send({"startDate": startDate})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	it("should read the reservations", (done) => {
		chai
			.request(app)
			.get("/reservations/readReservations")
			.set("content-type", "application/json")
			.send({})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});

	/*let newEndDate = "2024-01-09T16:59:52.876Z";
	it("should update the reservation", (done) => {
		chai
			.request(app)
			.post("/reservations/updateReservation/" + idUser + "/" + idMaterial)
			.set("content-type", "application/json")
			.send({
							"startDate": startDate,
							"endDate": newEndDate,
							"exStartDate": startDate,
							"exEndDate": endDate
    			  })
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});
	});*/

	it("should delete the reservation", (done) => {
		chai
			.request(app)
			.delete("/reservations/deleteReservation/" + userId + "/" + materialId)
			.set("content-type", "application/json")
			.send({
							"startDate": startDate,
							"endDate": endDate
						})
			.end((err, res) => {
				expect(res.status).to.equal(200);
				done();
			});

	});
});

/*******************
*	    IndexPage    *
*******************/
const mongoose = require("mongoose");
const appController = require("../controllers/app.controller.js");
var sandbox = sinon.createSandbox();


// ----------------------------------------------------------------------

describe("# smoke tests", function () {
    it("checks equality", function () {
      expect(true).to.be.true;
    });

    describe("getIndexPage", function () {
      it("should return index page", function () {
        let req = {}
        // Have `res` have a send key with a function value coz we use `res.send()` in our func
        let res = {
          send: sinon.spy()
        }

        appController.getIndexPage(req, res);
        expect(res.send.calledOnce).to.be.true;
        // expect to get argument `Hey` on first call
        expect(res.send.firstCall.args[0]).to.equal("Hey");
      });
    });
});
