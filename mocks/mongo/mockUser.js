module.exports.validUser = function () {
  return {
    "last_name": "nomTest",
    "first_name": "prenomTest",
    "mail_address": "test@test.test",
    "registration_number": "1234567",
    "password": "megaPassword",
    "role": "borrower"
  };
};

module.exports.validUser2 = function () {
  return {
    "last_name": "nomTest2",
    "first_name": "prenomTest2",
    "mail_address": "test2@test2.test2",
    "registration_number": "1234567",
    "password": "megaPassword2",
    "role": "borrower"
  };
};

module.exports.adminUser = function () {
  return {
    "last_name": "nomTestAdmin",
    "first_name": "prenomTestAdmin",
    "mail_address": "testAdmin@test.test",
    "registration_number": "1234567",
    "password": "megaPassword",
    "role": "administrator"
  };
};
module.exports.adminUser2 = function () {
  return {
    "last_name": "nomTestAdmin2",
    "first_name": "prenomTestAdmin2",
    "mail_address": "testAdmin2@test.test",
    "registration_number": "1234567",
    "password": "megaPassword",
    "role": "administrator"
  };
};

module.exports.invalidUser = function () {
  return {
    "last_name": "nomTest",
    "first_name": "prenomTest",
    "mail_address": "mail_invalide",
    "registration_number": "numero0",
    "password": "megaPassword",
    "role": "administrator"
  };
};

module.exports.modifiedUser = function () {
  return {
    "last_name": "superNomDeTest",
    "first_name": "meilleurPrenomDeTest"
  };
};