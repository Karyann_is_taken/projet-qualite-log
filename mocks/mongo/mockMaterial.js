module.exports.material1 = function () {
    return {
        "name": "Android FG",
        "version": "H319",
        "reference": "AN888",
        "borrowed": false
    };
};

module.exports.material2 = function () {
    return {
        "name": "iphone 15",
        "version": "15-2",
        "reference": "AP123",
        "photo": "labelfoto",
        "tel_number": "0606060606",
        "borrowed": false
    };
};

module.exports.invalidMaterial1 = function () {
    return {
        "name": "iphone 15",
        "version": "1",
        "reference": "A234215",
        "photo": "labelfoto",
        "tel_number": "0606060606",
        "borrowed": false
    };
};
