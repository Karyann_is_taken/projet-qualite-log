module.exports.reserv1 = function () {
    return {
        "material_id": "materialId1",
        "user_id": "userId1",
        "startDate": "2021-01-09T19:17",
        "endDate": "2021-01-10T19:17"
    };
};

module.exports.reserv2 = function () {
    return {
        "material_id": "materialId2",
        "user_id": "userId2",
        "startDate": "startDate2",
        "endDate": "endDate2"
    };
};

module.exports.invalidReserv1 = function () {
    return {
        "material_id": "materialId",
        // "user_id": userId, // invalide 
        "startDate": "2021-01-09T19:17",
        "endDate": "2021-01-10T19:17"
    };
};
