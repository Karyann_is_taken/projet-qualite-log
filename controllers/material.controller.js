// const db = require("../handler/db");
// const Material = db.Material;
const Material = require('../models/material');
//const app = require('../app');

//const db = app.mongoose;

//Crée un matériel et l'ajoute à la base de données
async function createMaterial(req, res) {
  return new Promise(function (resolve, reject) {
    const material = new Material({
      name: req.body.name,
      version: req.body.version,
      reference: req.body.reference,
      photo: req.body.photo,
      tel_number: req.body.tel_number,
      borrowed: req.body.borrowed
    });

    if ((/^([a-zA-Z0-9_-]){3,15}$/).test(material.version)
      && (/^(AN|AP|XX)\d\d\d$/).test(material.reference)
      && (material.tel_number == undefined
        || (material.tel_number !== undefined
          && /^([0-9]){10}$/.test(material.tel_number)))) {
      material.save()
        .then((result) => {
          resolve(result);
        })
        .catch((err) => {
          //console.log(err);
          resolve({ error: "badRequest" });
        });
    }
    else {
      resolve({ error: "badRequest" });
    }
  });
}

//Modifie les données d'un matériel dans la base de données
async function updateMaterial(req, res) {
  return new Promise(function (resolve, reject) {
    const material = new Material({
      _id: req.params.idMaterial,
      name: req.body.name,
      version: req.body.version,
      reference: req.body.reference,
      photo: req.body.photo,
      tel_number: req.body.tel_number,
      borrowed: req.body.borrowed
    });

    Material.findByIdAndUpdate(req.params.idMaterial, material)
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve({ error: "badRequest" });
      });
  });
}

//Récupère un matériel
async function readMaterial(req, res, material_id) {
  return new Promise(function (resolve, reject) {

    Material.findById(material_id)
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve({ error: "CouldntFind" });
      });
  });
}

//Récupère tous les matériels dans la base de données
async function readAllMaterial(req, res) {
  return new Promise(function (resolve, reject) {
    Material.find()
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve({ error: "CouldntFind" });
      });
  });
}

//Supprime un matériel de la base de données, supprime également les notifications, les emprutns et les réservations portant sur celui-ci
async function deleteMaterial(req, res, material_id) {
  return new Promise(async function (resolve, reject) {

    await Material.findByIdAndDelete(material_id)
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve({ error: "CouldntFind" });
      });

    var notifications = await notificationController.getNotificationsOfMaterial(req, res, material_id);
    if (notifications) {
      if (!notifications.error) {
        notifications.forEach(function (notif) {
          notificationController.deleteNotification(req, res, notif);
        });
      }
    }

    var reservations = await reservationController.getReservationsOfMaterial(req, res, material_id);
    if (reservations) {
      if (!reservations.error) {
        reservations.forEach(function (reserv) {
          reservationController.deleteReservation(req, res, reserv);
        });
      }
    }

    var borrows = await borrowController.getBorrowsOfMaterial(req, res, material_id);
    if (borrows) {
      if (!borrows.error) {
        borrows.forEach(async function (borrow) {
          await borrowController.deleteBorrow(req, res, borrow);
        });
      }
    }
  });
}

//Change le status "emprunté" d'un matériel
async function changeBorrowedStatus(req, res, material_id, borrowed) {

  return new Promise(function (resolve, reject) {
    Material.findById(material_id)
      .then((result) => {
        if (borrowed == undefined) {

          if (result.borrowed == true) {
            return result.updateOne({ borrowed: false });
          } else {
            return result.updateOne({ borrowed: true });
          }

        } else {
          if (borrowed == true) {
            return result.updateOne({ borrowed: true });
          } else {
            return result.updateOne({ borrowed: false });
          }
        }
      })
      .catch((err) => {
        resolve({ error: "CouldntFind" });
      });
  });
}


module.exports = {
  createMaterial,
  updateMaterial,
  readMaterial,
  readAllMaterial,
  deleteMaterial,
  changeBorrowedStatus
};

const reservationController = require('./reservation.controller');
const borrowController = require('./borrow.controller');
const notificationController = require('./notification.controller');
