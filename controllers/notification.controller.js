// const db = require("../handler/db");
// const Notification = db.Notification;
// const Reservation = db.Reservation;
const Notification = require('../models/notification');


//Crée une nouvelle notification et l'ajoute à la base de données
async function createNotification(req, res, notification) {
  return new Promise(function(resolve, reject) {
  if(userController.readUser(req, res, notification.user_id) !== undefined && materialController.readMaterial(req, res, notification.material_id)){
    notification.save()
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve( {error: "badRequest"});
      });
  }else{
    resolve( {error: "badRequest"});
  }
});
}

//Supprime une notification de la base de données
async function deleteNotification(req, res, notification) {
  return new Promise(function(resolve, reject) {
    Notification.findOneAndDelete({ material_id: notification.material_id, user_id: notification.user_id, notifDate: notification.notifDate, notifType: notification.notifType })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve( {error: "CouldntFind"});
    });
  });
}

//Trouve (récupère) les notifications générées par un utilisateur (emprunt) ou à la destination de cet utilisateur (disponibilité)
async function getNotificationsOfUser(req, res, user) {
  return new Promise(function(resolve, reject) {
    Notification.find({ user_id: user._id})
    .then((result) => {
      resolve(result);
    })
    .catch((err) => {
      //console.log(err);
      resolve( {error: "CouldntFind"});
    });
  });
}

//Trouve (récupère) toutes les notifications portant sur un matériel
async function getNotificationsOfMaterial(req, res, material) {
  return new Promise(function(resolve, reject) {
    Notification.find({ material_id: material._id})
    .then((result) => {
      resolve(result);
    })
    .catch((err) => {
      //console.log(err);
      resolve( {error: "CouldntFind"});
    });
  });
}

//Génère une notification d'emprunt
async function makeBorrowedNotification(req, res, borrow) {
  return new Promise(async function(resolve, reject) {

    var notification = new Notification({
      material_id: borrow.material_id,
      user_id: borrow.user_id,
      notifDate: new Date(),
      notifType: 'borrowed'
    });
    resolve(createNotification(req, res, notification));
  });
}

//Génère une notification de disponibilité
async function makeDisponibilityNotification(req, res, reservation) {
  return new Promise(async function(resolve, reject) {

  var notification = new Notification({
    material_id: reservation.material_id,
    user_id: reservation.user_id,
    notifDate: new Date(),
    notifType: 'disponible'
  });
  resolve(createNotification(req, res, notification));
  });
}

//Crée une notification de disponibilité pour tous les utilisateurs ayant réserver le matériel
async function notifyReservingUsersOfDisponibility(req, res, material_id) {
  return new Promise(async function(resolve, reject) {
    var reservations = await reservationController.getReservationsOfMaterialAfterDate(req, res, material_id, new Date());
    var newNotifications = [];
    if(reservations !== undefined){
      if(!reservations.error){
        reservations.forEach(async function(reserv){
          newNotifications.push(await makeDisponibilityNotification(req, res, {material_id: material_id, user_id: reserv.user_id}));
        });
      }
    }
    resolve(newNotifications);
  });
}

//Crée une notification d'emprunt pour tous les admins
async function notifyAdminOfBorrow(req, res, material_id) {
  return new Promise(async function(resolve, reject) {
    var admins = await userController.getAllAdmins(req, res);
    var newNotifications = [];
    if(admins !== undefined){
      if(!admins.error){
        admins.forEach(async function(admin){
          newNotifications.push(await makeBorrowedNotification(req, res, {material_id: material_id, user_id: admin.user_id}));
        });
      }
    }
    resolve(newNotifications);
  });
}

module.exports = {
  createNotification,
  deleteNotification,
  getNotificationsOfUser,
  makeBorrowedNotification,
  makeDisponibilityNotification,
  getNotificationsOfMaterial,
  notifyReservingUsersOfDisponibility,
  notifyAdminOfBorrow
};
const reservationController = require('./reservation.controller');
const userController = require('./user.controller');
const materialController = require('./material.controller');
