const db = require("../handler/db");
const Reservation = db.Reservation;

// const Reservation = require('../models/reservation');
// const User = require('../models/user');
// const Material = require('../models/material');


const cd = require('compare-dates');

//Création et insertion d'une réservation dans la base de données
async function createReservation(req, res) {
  const reservation = new Reservation({
    material_id: req.body.material_id,
    user_id: req.body.user_id,
    startDate: req.body.startDate,
    endDate: req.body.endDate
  });

    if(userController.readUser(req, res, reservation.user_id) !== undefined
      && materialController.readMaterial(req, res, reservation.material_id) !== undefined
      && cd.isAfter(reservation.startDate,new Date())
      && cd.isAfter(reservation.endDate, reservation.startDate)
      && !(await getReservationsOfMaterialBetweenDates(req, res, materialController.readMaterial(req, res, reservation.material_id),reservation.startDate, reservation.endDate).length)
    ){
      reservation.save()
        .then((result) => {
          res.send(result);
        })
        .catch((err) => {
          //console.log(err);
          res.send( {error: "BadRequest"});
        });
    }else{
      res.send( {error: "BadRequest"});
    }
}

//Lecture (récupération) d'une réservation puis la base de données
async function readReservation(req, res, reservation) {
  Reservation.find({ material_id: reservation.material_id, user_id: reservation.user_id, startDate: reservation.startDate, endDate: reservation.endDate })
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      //console.log(err);
      res.send( {error: "CouldntFind"});
    });
}

//Lecture (récupération) de toutes les réservations dans la base de données
async function readAllReservation(req, res) {
    Reservation.find()
        .then((result) => {
            res.send(result);
        })
        .catch((err) => {
            //console.log(err);
            res.send( {error: "couldntRead"});
        });
}

//Modification des données d'une réservation
async function updateReservation(req, res) {
  const update = {
    startDate: req.body.startDate,
    endDate: req.body.endDate
  };
  Reservation.findOneAndUpdate({ material_id: req.params.idMaterial, user_id: req.params.idUser, startDate: req.body.exStartDate, endDate: req.body.exEndDate}, update)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      //console.log(err);
      res.send( {error: "badRequest"});
    });
}

//Suppression d'une réservation de la base de données
async function deleteReservation(req, res, reservation) {
  if(reservation !== undefined){
    return new Promise(function(resolve, reject) {
    Reservation.findOneAndDelete({ material_id: reservation.material_id, user_id: reservation.user_id, startDate: reservation.startDate, endDate: reservation.endDate })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve( {error: "CouldntFind"});
      });
    });
  }else{
    return new Promise(function(resolve, reject) {
    Reservation.findOneAndDelete({ material_id: req.params.idMaterial, user_id: req.params.idUser, startDate: req.body.startDate, endDate: req.body.endDate })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve( {error: "CouldntFind"});
      });
    });
  }

}

//Trouve (récupère) toutes les réservations faites par un utilisateur
async function getReservationsOfUser(req, res, user_id) {

  return new Promise(function(resolve, reject) {
    Reservation.find({ user_id: user_id })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve( {error: "CouldntFind"});
      });
    });
}

//Trouve (récupère) toutes les réservations faites sur un matériel
async function getReservationsOfMaterial(req, res) {
  return new Promise(function(resolve, reject) {
  Reservation.find({ material_id: req.params.idMaterial})
    .then((result) => {
      resolve(result);
    })
    .catch((err) => {
      //console.log(err);
      resolve( {error: "CouldntFind"});
    });
  });
}

//Trouve toutes les réservations faites pour un matériel après une certaine date
async function getReservationsOfMaterialAfterDate(req, res, material, date) {
  return new Promise(function(resolve, reject) {
    Reservation.find({ material_id: material._id, startDate: {$gte: date} })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        resolve( {error: "CouldntFind"});
        //console.log(err);
    });
  });
}

//Trouve toutes les réservaitons qui sont au moins en partie sur une période de temps
async function getReservationsOfMaterialBetweenDates(req, res, material, startDate, endDate) {
  return new Promise(function(resolve, reject) {
  Reservation
    .find({
      material_id: material._id, $or: [
        { startDate: { $gte: startDate, $lt: endDate } },
        { endDate: { $gte: startDate, $lt: endDate } },
        { startDate: { $lt: startDate }, endDate: { $gte: endDate } }
      ]
    })
    .then((result) => {
      resolve(result);
    })
    .catch((err) => {
      //console.log(err);
      resolve( {error: "CouldntFind"});
    });
  });
}

module.exports = {
  createReservation,
  readReservation,
  readAllReservation,
  updateReservation,
  deleteReservation,
  getReservationsOfUser,
  getReservationsOfMaterial,
  getReservationsOfMaterialBetweenDates,
  getReservationsOfMaterialAfterDate
};

const userController = require('./user.controller');
const materialController = require('./material.controller');
