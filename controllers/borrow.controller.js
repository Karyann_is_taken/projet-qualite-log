const db = require("../handler/db");
const Borrow = db.Borrow;

//import { readUser, readMaterial, changeBorrowedStatus, notifyAdminOfBorrow, notifyReservingUsersOfDisponibility } from "../handler/internal.js";
// const Reservation = require('../models/reservation');
// const User = require('../models/user');
// const Material = require('../models/material');


//Crée une relation d'emprunt et l'ajoute à la base de données
async function createBorrow(req, res) {
  const borrow = new Borrow({
    material_id: req.body.material_id,
    user_id: req.body.user_id,
    startDate: Date.now(),
    endDate: undefined
  });

  if(userController.readUser(req, res, borrow.user_id) !== undefined && materialController.readMaterial(req, res, borrow.material_id)){
      borrow.save()
        .then((result) => {
          res.send(result);
        })
        .catch((err) => {
          console.log(err);
          res.send( {error: "badRequest"});
        });
  }else{
    res.send( {error: "badRequest"});
  }
}

//Récupère une relation d'emprunt depuis la base de données
async function readBorrow(req, res) {
  Borrow.find({ material_id: req.params.idMaterial, user_id: req.params.idUser, startDate: req.body.startDate})
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      //console.log(err);
      res.send( {error: "CouldntFind"});
    });
}

//Récupère toutes les relations d'emprunt de la base de données
async function readAllBorrow(req, res) {
    Borrow.find()
        .then((result) => {
            res.send(result);
        })
        .catch((err) => {
            //console.log(err);
            res.send( {error: "CouldntRead"});
        });
}

//Modifie la date de fin d'une relation d'emprunt
async function updateBorrow(req, res, borrow) {
  return new Promise(function(resolve, reject) {
    if(borrow !== undefined){

        const update = {
          /*
          material_id: borrow.material_id,
          user_id: borrow.user_id,
          startDate: borrow.startDate,*/
          endDate: borrow.endDate
        };
      Borrow.findOneAndUpdate({material_id: borrow.material_id, user_id: borrow.user_id, startDate: borrow.startDate}, update)
          .then((result) => {
            resolve(result);
          })
          .catch((err) => {
            //console.log(err);
            resolve( {error: "badRequest"});
        });
    }else{
        const update = {
          material_id: req.body.material_id,
          user_id: req.body.user_id,
          startDate: req.body.startDate,
          endDate: req.body.endDate
        };
      Borrow.findOneAndUpdate({material_id: req.body.material_id, user_id: req.body.user_id, startDate: req.body.startDate}, update)
          .then((result) => {
            resolve(result);
          })
          .catch((err) => {
            //console.log(err);
            resolve( {error: "badRequest"});
        });
      }
    });
  }

//Supprime une relation d'emprunt de la base de données
async function deleteBorrow(req, res, borrow) {

  return new Promise(function(resolve, reject) {
    if(borrow == undefined){
      Borrow.findOneAndDelete({ material_id: req.params.idMaterial, user_id: req.params.idUser, startDate: req.body.startDate})
        .then((result) => {
          console.log("Emprunt supprimé");
          resolve(result);
        })
        .catch((err) => {
          //console.log(err);
          resolve( {error: "CouldntFind"});
        });
    }else{
      Borrow.findOneAndDelete({ material_id: borrow.material_id, user_id: borrow.user_id, startDate: borrow.startDate})
        .then((result) => {
          console.log("Emprunt supprimé");
          resolve(result);
        })
        .catch((err) => {
          //console.log(err);
          resolve( {error: "CouldntFind"});
        });
    }
  });
}

//Trouve la relation d'emprunt encore en cours d'un matériel
async function getCurrentBorrower(req, res, material_id) {
  return new Promise(function(resolve, reject) {
    Borrow.find({ material_id: material_id, endDate: undefined})
    .then((result) => {
      resolve(result);
    })
    .catch((err) => {
      //console.log(err);
      resolve( {error: "CouldntFind"});
    });
  });
}

//Trouve la liste des emprunts d'un utilisateur
async function getBorrowsOfUser(req, res, user_id) {

  return new Promise(function(resolve, reject) {
    Borrow.find({ user_id: user_id })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve( {error: "CouldntFind"});
    });
  });
}

//Trouve tous les emprunts portant sur un matériel dans la base de données
async function getBorrowsOfMaterial(req, res, material) {
  return new Promise(function(resolve, reject) {

  Borrow.find({ material_id: material._id })
    .then((result) => {
      resolve(result);
    })
    .catch((err) => {
      resolve( {error: "CouldntFind"});
    });
  });
}

//Emprunte un matériel
async function borrowingOfMaterial(req, res, material_id, user_id) {
  const newBorrow = new Borrow({
    material_id: material_id,
    user_id: user_id,
    startDate: new Date()
  });
  createBorrow(req, res, newBorrow);
  materialController.changeBorrowedStatus(req, res, material_id, true);
  notificationController.notifyAdminOfBorrow(req, res, material_id);
}

//Rend un matériel
async function returnMaterial(req, res, material_id) {
  const currentBorrow = getCurrentBorrower(req, res, material_id);

  const newBorrow = new Borrow({
    material_id: currentBorrow.material_id,
    user_id: currentBorrow.user_id,
    startDate: currentBorrow.startDate,
    endDate: new Date()
  });
  res.send(updateBorrow(req, res, newBorrow));
  materialController.changeBorrowedStatus(req, res, material_id, false);
  notificationController.notifyReservingUsersOfDisponibility(req, res, material_id);
}

module.exports = {
  createBorrow,
  readBorrow,
  readAllBorrow,
  updateBorrow,
  deleteBorrow,
  getBorrowsOfUser,
  getBorrowsOfMaterial,
  getCurrentBorrower,
  borrowingOfMaterial,
  returnMaterial
};

const userController = require('./user.controller');
const materialController = require('./material.controller');
const notificationController = require('./notification.controller');
