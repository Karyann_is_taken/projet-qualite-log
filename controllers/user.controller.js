// const db = require("../handler/db");
// const User = db.User;
const User = require('../models/user');
//const app = require('../app');




//Creation d'un utilisateur dans la base de donnée
async function createUser(req, res) {
  return new Promise(function (resolve, reject) {
    const user = new User({
      last_name: req.body.last_name,
      first_name: req.body.first_name,
      mail_address: req.body.mail_address,
      registration_number: req.body.registration_number,
      password: req.body.password,
      role: req.body.role
    });

    if ((/^[^@\s]+@[^@\s]+\.[^@\s]+$/).test(user.mail_address)
      && (/^([a-zA-Z0-9_-]){7}$/).test(user.registration_number)
      && (/^(administrator|borrower)$/).test(user.role)) {
      user.save()
        .then((result) => {
          // res.send(result);
          resolve(result);
        })
        .catch((err) => {
          //console.log(err);
          // res.send({ error: "BadRequest" });
          resolve({ error: "BadRequest" });
        });
    } else {
      // res.send({ error: "badRequest" });
      resolve({ error: "badRequest" });
    }
  });
}

//Changement des données d'un utilisateur dans la base de données
async function updateUser(req, res) {
  return new Promise(function (resolve, reject) {
    const user = new User({
      _id: req.params.idUser,
      last_name: req.body.last_name,
      first_name: req.body.first_name,
      mail_address: req.body.mail_address,
      registration_number: req.body.registration_number,
      password: req.body.password,
      role: req.body.role
    });

    if ((/^[^@\s]+@[^@\s]+\.[^@\s]+$/).test(user.mail_address)
      && (/^([a-zA-Z0-9_-]){7}$/).test(user.registration_number)
      && (/^(administrator|borrower)$/).test(user.role)) {
      User.findByIdAndUpdate(req.params.idUser, user)
        .then((result) => {
          // res.send(result);
          resolve(result);
        })
        .catch((err) => {
          //console.log(err);
          // res.send({ error: "BadRequest" });
          resolve({ error: "BadRequest" });
        });
    } else {
      resolve({ error: "badRequest" });
    }
  });
}

//Lecture (récupération) d'un utilisateur selon son ID depuis la base de données
async function readUser(req, res, user_id) {
  return new Promise(function (resolve, reject) {
    User.findById(user_id)
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve({ error: "CouldntFindUser" });
      });
  });
}

//Lecture (récupération) de tous les utilisateurs dans la base de données
async function readAllUser(req, res) {
  return new Promise(function (resolve, reject) {
    User.find()
      .then((result) => {
        // res.send(result);
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        // res.send({ error: "CouldntRead" });
        resolve({ error: "CouldntRead" });
      });
  });
}

//Lecture (récupération) de tous les utilisateurs au status d'administrateur dans la base de données
async function getAllAdmins(req, res) {
  return new Promise(function (resolve, reject) {
    User.find({ role: "administrator" })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve({ error: "CouldntRead" });
      });
  });
}

//Suppression d'un utilisateur de la base de données. Supprime également les réservations dont il est à l'origine, les emprunts en son nom ainsi que les notifications qu'il a reçu/émis (=> intégrité des données)
async function deleteUser(req, res, user_id) {
  await User.findByIdAndDelete(user_id)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      //console.log(err);
      res.send({ error: "CouldntFindUser" });
    });

  var notifications = await notificationController.getNotificationsOfUser(req, res, user_id);
  if (notifications) {
    if (!notifications.error) {
      notifications.forEach(function (notif) {
        notificationController.deleteNotification(req, res, notif);
      });
    }
  }

  var reservations = await reservationController.getReservationsOfUser(req, res, user_id);
  if (reservations) {
    if (!reservations.error) {
      reservations.forEach(function (reserv) {
        reservationController.deleteReservation(req, res, reserv);
      });
    }
  }

  var borrows = await borrowController.getBorrowsOfUser(req, res, user_id);
  if (borrows) {
    if (!borrows.error) {
      borrows.forEach(async function (borrow) {
        await borrowController.returnMaterial(req, res, borrow);
        await borrowController.deleteBorrow(req, res, borrow);
      });
    }
  }
}

//Vérifie que l'adresse mail et le mot de passe donnés permettent d'identifier un utilisateur
async function confirmIdentity(req, res, mail_address, password) {
  return new Promise(function (resolve, reject) {
    User.findOne({ mail_address: mail_address, password: password })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        //console.log(err);
        resolve({ error: "CoulntIdentify" });
      });
  });
}


module.exports = {
  createUser,
  updateUser,
  readUser,
  readAllUser,
  deleteUser,
  confirmIdentity,
  getAllAdmins
};

const reservationController = require('./reservation.controller');
const borrowController = require('./borrow.controller');
const notificationController = require('./notification.controller');
