# Projet d’application de gestion d’emprunt de matériel informatique 

## A propos

Site web permettant de gérer les emprunts du matériel informatique de la société locaMat. Celui-ci est destiné à un usage interne.



## Installation

1. `npm install` pour installer les dependances.

2. `npm start` pour démarrer le serveur. 
Le serveur démarre à l'adresse http://localhost:3000/ et utilise une base de données mongoDB en localhost (mongodb://localhost:27017/ProjetQualiteLogicielle).

3. `npm run devstart` pour démarrer le serveur et qu'il redémarre à chaque changement dans le code source.

4. `npm run test` pour lancer les tests d'integrations et unitaires ainsi qu'un rapport de couverture du code.



## Fonctionnalités

| Fonctionnalité                         | Administrateur | Utilisateur | Invité |
| -------------------------------------- |:--------------:|:-----------:|:------:|
|                                        |                |             |        |
| Se connecter                           |                |             |   X    |
| Inscrire un utilisateur/administrateur |       X        |             |        |
| Consulter les utilisateurs             |       X        |             |        |
| Modifier un utilisateur                |       X        |             |        |
| Supprimer un utilisateur               |       X        |             |        |
|                                        |                |             |        |
| Consulter les matériels                |       X        |      X      |   X    |
| Consulter un matériel en détail        |       X        |      X      |   X    |
| Réserver un matériel                   |       X        |      X      |        |
| Supprimer une réservation              |       X        |             |        |
| Définir un matériel comme emprunté     |       X        |             |        |
| Mettre fin à un emprunt                |       X        |             |        |
| Modifier un matériel                   |       X        |             |        |
| Supprimer un matériel                  |       X        |             |        |




## Test fonctionnel avec Selenium IDE : 

1. S'assurer que la base de données est vide.

2. Effectuer la requête suivante dans Postman:

```js
POST http://localhost:3000/users/addUser

body : 
{
    "last_name":"root",
    "first_name":"root",
    "mail_address":"root@root.root",
    "registration_number":"7777777",
    "password":"root",
    "role":"administrator"
}
```

3. Le test devrait :
    - Se connecter avec l'utilisateur root@root.root
    - Créer un nouvel utilisateur
    - Se déconnecter
    - Se connecter avec le nouvel utilisateur crée
    - Consulter la liste de matériels
    - Se déconnecter
    - Se connecter en tant que root
    - Supprimer l'utilisateur précedemment ajouté
    - Se déconnecter

Rappel : Il faut manuellement appuyer sur les boutons OK des alertes.
